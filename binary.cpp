#include <iostream>
#include <fstream>
using namespace std;

int main( )
{
   //create some data
   short int a = -6730;
   float b = 68.123; 
   char c = 'J';

	ofstream in{"abc.bin", ios::binary};
	
 if(in){
	 
	in.write(reinterpret_cast<char*>(&a), sizeof(a));
	in.write(reinterpret_cast<char*>(&b), sizeof(b));
	in.write(reinterpret_cast<char*>(&c), sizeof(c));

   cout<<endl<<endl;
 
   in.close();
}
}
